'use strict';

angular
  .module('expenseManagerAppApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize'
  ]);
